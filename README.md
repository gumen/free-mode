# free-mode

Very simple major mode for `free -h` program output.

![free-mode screenshot](./screenshot.png "free-mode screenshot")

`M-x free<RET>` to run  
hit `g` to refresh  
`q` to quit and that's it!
