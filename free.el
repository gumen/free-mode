;;; free-mode
;; Print tabulated list using output from `free -h'.
;; M-x free<RET> to run; hit g to refresh; q to quit and that's it!
;; - gumen (2020-02-16 11:43:22 CET)

(define-derived-mode free-mode tabulated-list-mode "Free"
  "Print tabulated list using output from `free -h'."
  (let* ((lines (split-string (shell-command-to-string "free -h") "\n" t "\s"))
         (values (seq-map (lambda (line) (split-string line "\s" t "\s")) lines))
         (header (cons "" (elt values 0)))
         (column-width (/ (min (window-width) 70) (length header)))
         (format (vconcat (seq-map (lambda (c) (list c column-width)) header)))
         (memory (list "memory" (vconcat (elt values 1))))
         (swap (list "swap" (vconcat (append (elt values 2) (make-list 3 ""))))))
    (setq tabulated-list-format format)
    (setq tabulated-list-entries (list memory swap)))
  (tabulated-list-init-header)
  (tabulated-list-print)
  (buffer-disable-undo)
  (run-mode-hooks 'free-mode-hook))

(defvar free-mode-map (make-sparse-keymap) "`free-mode' keymap")

(define-key free-mode-map (kbd "g") 'free-mode)

(defun free ()
  "Run `free-mode' in `*Free*' buffer."
  (interactive)
  (pop-to-buffer (get-buffer-create "*Free*"))
  (free-mode))

(provide 'free)
